# (HA)²proxy

### An Ansible swiss-army-knife to configure Highly Available HAProxy


This toolkit aims to provide a reliable and easy way to set up
and maintain a cluster of HAProxy proxies but can also work
with a single node.

# Infrastructure

## Minimum setup

A single host with public IP address used as primary.

## Suggested setup

- 1x HAProxy primary host
- 1 or more HAProxy secondary host(s)
